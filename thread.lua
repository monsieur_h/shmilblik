mainThread = MOAIThread.new()
mainThread:run(function()
    currentTime = MOAISim.getDeviceTime()
    while true do
        updateCameraPos(playerBall:getPosition())
        if isGameOver() then
            GAME.setState("LOOSE")
        end
        --
        --Updating animation
        PLAYER.SKELETON.animationState:update(MOAISim.getStep())
        PLAYER.SKELETON.animationState:apply(PLAYER.SKELETON)
        PLAYER.SKELETON:updateWorldTransform()
        --
        --Centering the Spine animation on the player body
        local x, y = playerBall:getPosition()
        local a = playerBall:getAngle()
        PLAYER.SKELETON.bones[1].rotation = a
        PLAYER.SKELETON.prop:setLoc(x, y)
        --
        --Changing animation if it is over
        local pl = PLAYER.SKELETON
        if pl.animationState.currentTime >= pl.animationState.current.duration then
            if pl.animationState.current.name == "hit" then
                pl.animationState:setAnimation("fly", false)
            elseif pl.animationState.current.name == "win" then
                pl.animationState:setAnimation("idle", true)
            elseif pl.animationState.current.name == "fly" then
                pl.animationState:setAnimation("idle", true)
            end
        end
        local previousTime = currentTime
        currentTime = MOAISim.getDeviceTime()
        DELTATIME = currentTime - previousTime
        updateRays(DELTATIME, playerBall:getPosition())
        coroutine.yield()
    end
end)

--==========
--LEVEL FILE
--LEVEL 6
--==========
LEVEL = {
    SCORE = 2,
    NUMBER = 6,
    WALLS = {
        { x = -150, y = 100, len = 200, wid = 50, angle = 0, mode = "redbox" },
        { x = 250, y = 200, len = 200, wid = 50, angle = -90, mode = "box" },
    },
    CHECKPOINTS = {},
    END = { x = -250, y = 250, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "I don't like the color of this block."
initLevel()

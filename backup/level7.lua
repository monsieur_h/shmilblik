--==========
--LEVEL FILE
--LEVEL 7
--First level with a magnet block
--==========
LEVEL = {
    SCORE = 2,
    NUMBER = 7,
    WALLS = {
        { x = 0, y = 100, len = 200, wid = 50, angle = 45, mode = "magnet" },
        { x = 450, y = 150, len = 200, wid = 50, angle = -90, mode = "box" },
    },
    CHECKPOINTS = {},
    END = { x = 200, y = 350, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "Look ! A new block ! I wonder what it does !"
initLevel()

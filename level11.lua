--==========
--LEVEL FILE
--LEVEL 11
--Kind of a tutorial
--==========
LEVEL = {
    SCORE = 0,
    NUMBER = 11,
    WALLS = {
        { x = -500, y = 200, len = 200, wid = 50, angle = -90 },
        { x = -500, y = 00, len = 200, wid = 50, angle = -90 },
        { x = 500, y = 500, len = 200, wid = 50, angle = 45, mode = "redbox" },
        { x = 300, y = 300, len = 200, wid = 50, angle = 45, mode = "redbox" },

        { x = 500, y = 900, len = 200, wid = 50, angle = 45, mode = "redbox" },
        { x = 300, y = 700, len = 200, wid = 50, angle = 45, mode = "redbox" },
    },
    CHECKPOINTS = {},
    END = { x = 400, y = 700, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "Spicky Hole"
--LEVEL.BACKGROUND    =   "0.png"
initLevel()







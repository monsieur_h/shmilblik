--==========
--LEVEL FILE
--LEVEL 1
--==========
LEVEL = {
    NUMBER = 1,
    SCORE = 1,
    WALLS = {
        { x = -150, y = 100, len = 200, wid = 50, angle = 90 }, --A
        { x = -150, y = 300, len = 200, wid = 50, angle = 90 }, --A
        { x = -150, y = 500, len = 200, wid = 50, angle = 90 }, --A
        --{x=150, y=100, len=200, wid=50, angle=90},    --A
        --{x=150, y=300, len=200, wid=50, angle=90},    --A
        { x = 150, y = 500, len = 200, wid = 50, angle = 90 } --B
    },
    CHECKPOINTS = {},
    END = { x = -100, y = 650, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = ""
LEVEL.BACKGROUND = "1.png"
initLevel()
log("Done loading level 0")

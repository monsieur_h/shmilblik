--==========
--LEVEL FILE
--LEVEL 1
--==========
LEVEL = {
    NUMBER = 2,
    SCORE = 2,
    WALLS = {
        { x = -250, y = -200, len = 200, wid = 50, angle = 90 }, --A
        { x = -250, y = 0, len = 200, wid = 50, angle = 90 }, --A
        { x = -250, y = 200, len = 200, wid = 50, angle = 90 }, --A
        { x = -250, y = 400, len = 200, wid = 50, angle = 90 }, --A
        { x = -250, y = 600, len = 200, wid = 50, angle = 90 }, --A
        { x = -250, y = 800, len = 200, wid = 50, angle = 90 }, --A
        { x = -250, y = 1000, len = 200, wid = 50, angle = 90 }, --A
    },
    CHECKPOINTS = {},
    END = { x = -275, y = 1200, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "Tap the top left corner or right click to retry"
LEVEL.BACKGROUND = "2.png"
initLevel()

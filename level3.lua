--==========
--LEVEL FILE
--LEVEL 3
--==========
LEVEL = {
    SCORE = 3,
    NUMBER = 3,
    WALLS = {
        --{x=-150, y=-100, len=200, wid=50, angle=90},    --A
        --{x=-150, y=100, len=200, wid=50, angle=90},    --A
        { x = -450, y = 300, len = 200, wid = 50, angle = 90 }, --A
        { x = -300, y = 500, len = 200, wid = 50, angle = 90 }, --A
        { x = -150, y = 700, len = 200, wid = 50, angle = 90 }, --A
        { x = -150, y = 1000, len = 200, wid = 50, angle = 90 }, --A
    },
    CHECKPOINTS = {},
    END = { x = -200, y = 1300, len = 200, wid = 100, angle = 0 },
}
LEVEL.BACKGROUND = "3.png"
LEVEL.INTRO_TEXT = ""
LEVEL.FIXTURES = {}
initLevel()

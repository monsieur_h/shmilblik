--==========
--LEVEL FILE
--LEVEL 10
--Kind of a tutorial
--==========
LEVEL = {
    SCORE = 0,
    NUMBER = 10,
    WALLS = {
        { x = -175, y = 100, len = 200, wid = 50, angle = -90 },
        { x = -175, y = -100, len = 200, wid = 50, angle = -90 },
        { x = -175, y = -300, len = 200, wid = 50, angle = -90 },
        { x = -50, y = 200, len = 200, wid = 50, angle = 0, mode = "redbox" },
        { x = 100, y = 200, len = 200, wid = 50, angle = 45 },
        { x = -600, y = 200, len = 200, wid = 50, angle = 90 },
        { x = -600, y = 400, len = 200, wid = 50, angle = 90 },
    },
    CHECKPOINTS = {},
    END = { x = -500, y = 00, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "This one is very high"
--LEVEL.BACKGROUND    =   "0.png"
initLevel()






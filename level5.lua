--==========
--LEVEL FILE
--LEVEL 5
--==========
LEVEL = {
    SCORE = 2,
    NUMBER = 5,
    WALLS = {
        { x = -125, y = 200, len = 200, wid = 50, angle = -45 },
        { x = 105, y = 200, len = 200, wid = 50, angle = 225 },
        { x = 350, y = 100, len = 200, wid = 50, angle = 90 },
        { x = 350, y = 300, len = 200, wid = 50, angle = 90 },
    },
    CHECKPOINTS = {},
    END = { x = -180, y = 600, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "Does it looks like a choice to you?"
initLevel()

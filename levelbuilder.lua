boxDeck = MOAIGfxQuad2D.new()
boxDeck:setTexture("assets/box.png")
boxDeck:setRect(-256, -64, 256, 64)

redBoxDeck = MOAIGfxQuad2D.new()
redBoxDeck:setTexture("assets/red-box.png")
redBoxDeck:setRect(-256, -64, 256, 64)

magnetDeck = MOAIGfxQuad2D.new()
magnetDeck:setTexture("assets/magnet.png")
magnetDeck:setRect(-256, -64, 256, 64)

finishDeck = MOAIGfxQuad2D.new()
finishDeck:setTexture("assets/finish.png")
finishDeck:setRect(0, -128, 512, 128)

function initLevel()
    for i, v in pairs(LEVEL.WALLS) do
        createWall(v.x, v.y, v.len, v.wid, v.angle, v.mode)
    end
    createCheckPoints()
    if LEVEL.BACKGROUND then
        print("BACKGROUND")
        loadBackground(LEVEL.BACKGROUND)
    end
end

function loadBackground(pFile)
    print("Loading background file : " .. pFile)
    bgTexture = MOAIGfxQuad2D.new()
    bgTexture:setTexture("assets/lvl_bg/" .. pFile)
    bgTexture:setRect(-640, -384, 640, 384)
    backGroundObject = MOAIProp2D.new()
    backGroundObject:setDeck(bgTexture)
    backGroundObject:setLoc(0, 0)
    --backGroundObject:setScl( 0.5, 0.5 )
    foregroundLayer:insertProp(backGroundObject)
    LEVEL.BACKGROUND = backGroundObject
end

function destroyLevel()
    --The platforms
    for i, v in pairs(LEVEL.FIXTURES) do
        if v:getBody() then
            layer:removeProp(v:getBody().prop)
            v:getBody():destroy()
        end
    end
    --The goal
    --LEVEL.END.FIXTURE:getBody():destroy()
    layer:removeProp(LEVEL.END.prop)
    LEVEL.END.BODY:destroy()
    LEVEL.END.FIXTURE:destroy()
    if LEVEL.BACKGROUND then
        foregroundLayer:removeProp(LEVEL.BACKGROUND)
    end

    log("End Point destroyed")
end

function wallCollisionHandler(event, fixA, fixB, arbiter)
    if event == MOAIBox2DArbiter.END then
    end
end

function createCheckPoints()
    createFinnishLine(LEVEL.END.x, LEVEL.END.y, LEVEL.END.len, LEVEL.END.wid, LEVEL.END.angle)
end


function createFinnishLine(pX, pY, pLen, pWid, pAngle)
    if not world then
        return
    end
    local body = world:addBody(MOAIBox2DBody.STATIC)
    local fixture = body:addRect(0, -pWid / 2, pLen, pWid / 2)
    fixture:setSensor(true)
    body:setTransform(pX, pY, pAngle)
    fixture:setCollisionHandler(endPointListener)

    LEVEL.END.FIXTURE = fixture
    LEVEL.END.BODY = body

    LEVEL.END.prop = MOAIProp2D.new()
    LEVEL.END.prop:setScl(0.41, 0.41)
    LEVEL.END.prop:setDeck(finishDeck)
    LEVEL.END.prop:setParent(LEVEL.END.BODY)
    LEVEL.END.prop:setColor(1, 1, 1, 1)
    layer:insertProp(LEVEL.END.prop)
    log("Created")
end

function endPointListener(event, fixA, fixB, arbiter)
    if fixB.tag == "player" then
        GAME.setState("WIN")
    end
end

function createWall(pX, pY, pLen, pWid, pAngle, pType)
    if not world then
        return
    end
    pType = pType or "box"
    local body = world:addBody(MOAIBox2DBody.STATIC)
    local fixture = body:addRect(-pLen / 2, -pWid / 2, pLen / 2, pWid / 2)
    --local fixture   =   body:addRect( -pLen/2, -pWid/2, pLen/2, pWid/2 )
    fixture:setRestitution(0.5)
    fixture:setFriction(0.8)
    fixture:setFilter(0x1)
    body:resetMassData()

    body:setTransform(pX, pY, pAngle)

    body.prop = MOAIProp2D.new()
    --Specific for each type of platform
    --Adding a image to fit it
    if pType == "box" then
        body.prop:setDeck(boxDeck)
        body.prop:setScl(0.40, 0.40)
        body.prop:setParent(body)
        body.prop.tag = "level"
        fixture.tag = "level"
    elseif pType == "redbox" then
        body.prop:setDeck(redBoxDeck)
        body.prop:setScl(0.4, 0.4)
        body.prop:setParent(body)
        body.prop.tag = "redbox"
        fixture.tag = "redbox"
    elseif pType == "magnet" then
        body.prop:setDeck(magnetDeck)
        body.prop:setScl(0.4, 0.4)
        body.prop:setParent(body)
        body.prop.tag = "magnet"
        fixture.tag = "magnet"
    end

    layer:insertProp(body.prop)

    table.insert(LEVEL.FIXTURES, fixture)
end

function bumpwall(pBody)
    local t = MOAIThread.new()
    t:run(_bumpWall, pBody.prop)
end

function _bumpWall(pProp)
    if pProp.tag == "redbox" then
        local x, y = pProp:getScl()
        local pivX, pivY = pProp:getPiv()
        wait(pProp:seekColor(1.5, 1.5, 1.5, 1, 0.01))
        wait(pProp:seekColor(1, 1, 1, 1, 0.5))
    elseif pProp.tag == "magnet" then
        wait(pProp:moveRot(180, 0.5))
    else
        log("no tag found, letś go !")
        wait(pProp:seekColor(0.5, 1, 0.5, 0.1, 0.01))
        wait(pProp:seekColor(1, 1, 1, 1, 0.5))
    end
end

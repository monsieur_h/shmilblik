--==========================
--File handling the controls for both mouse and touch inputs
--==========================

PLAYER = {
    SCORE = 0,
    AMMO = 1,
    BODIES = {},
    SKELETON = {},
    SOUNDS = {},
}

function PLAYER.loadSounds()
    MOAIUntzSystem.initialize()
    MOAIUntzSystem.setVolume(1)
    win = MOAIUntzSound.new()
    win:load("assets/win.wav")

    loose = MOAIUntzSound.new()
    loose:load("assets/boo.wav")

    hitbar = MOAIUntzSound.new()
    hitbar:load("assets/hit_bar.wav")

    hitblock = MOAIUntzSound.new()
    hitblock:load("assets/hit_block.wav")

    barplace = MOAIUntzSound.new()
    barplace:load("assets/bar_placement.wav")


    PLAYER.SOUNDS.WIN = win
    PLAYER.SOUNDS.HITBLOCK = hitblock
    PLAYER.SOUNDS.HITBAR = hitbar
    PLAYER.SOUNDS.LOOSE = loose
    PLAYER.SOUNDS.BARPLACE = barplace
end

function PLAYER.setActive(pActive)
    playerBall:setActive(pActive)
    for i, v in pairs(PLAYER.BODIES) do
        v:setActive(pActive)
    end
    leftArmBody:setActive(pActive)
    rightArmBody:setActive(pActive)
    playerBall:setActive(pActive)
end

function PLAYER.reset()
    PLAYER.setActive(false)
    for i, v in pairs(PLAYER.BODIES) do
        v:setLinearVelocity(0.0)
        v:setAngularVelocity(0.0)
        local p = v.initialPosition
        v:setTransform(0, 0, 0)
    end
    playerBall:setTransform(0, 0, 0)
end

function drawControlDebugLines(index, xOff, yOff, xScale, yScale)
    local x2, y2 = 0, 0
    if MOAIInputMgr.device.pointer then
        x2, y2 = camera:worldToModel(worldX, worldY)
    else
        x2, y2 = MOAIInputMgr.device.touch:getTouch()
        x2, y2 = convertToSmartphoneSpace(x2, y2)
        x2, y2 = camera:worldToModel(x2, y2)
    end
    if clickedPoint then
        local x1, y1 = camera:worldToModel(clickedPoint.x, clickedPoint.y)
        local line = {
            x1, y1,
            x2, y2
        }
        MOAIGfxDevice.setPenColor(0, 0, 0, 1)
        MOAIGfxDevice:setPenWidth(5)
        MOAIDraw.drawLine(line)
        MOAIDraw.drawCircle(x2, y2, 45, 45)
        MOAIDraw.drawCircle(x1, y1, 45, 45)
    end
    --if MOAIInputMgr.device.touch and MOAIInputMgr.device.touch:isDown() then
    --MOAIDraw.drawCircle( x2, y2, 45, 45 )
    --MOAIDraw.drawCircle( clickedPoint.x, clickedPoint.y, 45, 45 )
    --end
end

debugControl = MOAIScriptDeck.new()
debugControl:setRect(-Stage.w / 2, -Stage.h / 2, Stage.w / 2, Stage.h / 2)
debugControl:setDrawCallback(drawControlDebugLines)

mouseLine = MOAIProp2D.new()
mouseLine:setDeck(debugControl)
hudLayer:insertProp(mouseLine)

function clickHandler(pDown, pX, pY)
    if GAME.STATE == "LOOSE" then
        log("Click in LOOSE state")
        GAME.setState("RESTART")
        return
    end
    if GAME.STATE == "RESTART" then
        log("Click in RESTART state")
        GAME.setState("RUN")
        return
    end
    if GAME.STATE == "WIN" then
        log("Click in WIN state")
        if pDown then
            GAME.setState("NEXTLEVEL")
        end
        return
    end

    if pDown then
        if (pX < -470)
                and (pY > 250) then
            log("reset buttonnnnnnn1!!!")
            GAME.setState("RESTART")
            return
        end
        if PLAYER.AMMO > 0 then
            clickedPoint = { x = pX, y = pY }
            if next(PLATFORMS) ~= nil then
                removePlayerPlatforms()
            end
        end
    else
        if clickedPoint then
            PLAYER.SOUNDS.BARPLACE:stop()
            PLAYER.SOUNDS.BARPLACE:play()
            createPlatform(clickedPoint.x, clickedPoint.y, pX, pY)
            clickedPoint = nil
            updateText()
            if not playerBall:isActive() then
                --playerBall:setActive( true )
                PLAYER.setActive(true)
                --GAME.setState( "RUN" )
            end
        end
    end
end

--Callback function for mouse
function onMouseLeftEvent(mousedown)
    clickHandler(mousedown, worldX, worldY)
end

function onMouseRightEvent(pMousedown)
    GAME.setState("RESTART")
end

--Callback function for touch device
function onTouchEvent(eventType, idx, x, y, tapCount)
    if (eventType == MOAITouchSensor.TOUCH_DOWN) then
        local x1, y1 = convertToSmartphoneSpace(x, y)
        clickHandler(true, x1, y1)
    elseif (eventType == MOAITouchSensor.TOUCH_UP) then
        local x1, y1 = convertToSmartphoneSpace(x, y)
        clickHandler(false, x1, y1)
    end
end

--Function to track pointer position
function pointerCallback(x, y)
    worldX, worldY = layer:wndToWorld(x, y)
end


--Setting callbacks depending on the device found
if MOAIInputMgr.device.pointer then
    print("Found a MOUSE pointer, setting callbacks...")
    MOAIInputMgr.device.mouseLeft:setCallback(onMouseLeftEvent)
    MOAIInputMgr.device.mouseRight:setCallback(onMouseRightEvent)
    MOAIInputMgr.device.pointer:setCallback(pointerCallback)
else
    print("Found a TOUCH device, setting callbacks...")
    MOAIInputMgr.device.touch:setCallback(onTouchEvent)
end

function convertToSmartphoneSpace(pX, pY)
    local x = pX - Stage.w / 2
    local y = (pY - Stage.h / 2) * -1
    local camx, camy = camera:getLoc()
    return x + camx, y + camy
end

function isGameOver()
    local x, y = playerBall:getPosition()
    if x >= Stage.w / 2
            or x <= -Stage.w / 2
            --or y >= Stage.h/2
            or y <= -Stage.h / 2
    then
        return true
    else
        return false
    end
end

worldX, worldY = 0, 0 --init thoses

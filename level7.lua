--==========
--LEVEL FILE
--LEVEL 7
--Kind of a tutorial
--==========
LEVEL = {
    SCORE = 0,
    NUMBER = 7,
    WALLS = {
        { x = -150, y = 0, len = 200, wid = 50, angle = 90, mode = "redbox" }, --A
        { x = -150, y = -400, len = 200, wid = 50, angle = 90, mode = "redbox" }, --A
        { x = -150, y = 200, len = 200, wid = 50, angle = 90, mode = "redbox" }, --A
        { x = -25, y = 300, len = 200, wid = 50, angle = 0, mode = "redbox" }
    },
    CHECKPOINTS = {},
    END = { x = -500, y = 0, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
--LEVEL.BACKGROUND    =   "0.png"
initLevel()



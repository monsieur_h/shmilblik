--==========
--LEVEL FILE
--LEVEL 4
--==========
LEVEL = {
    SCORE = 2,
    NUMBER = 4,
    WALLS = {
        { x = -150, y = -100, len = 200, wid = 50, angle = 90 }, --A
        { x = -150, y = 100, len = 200, wid = 50, angle = 90 }, --A
        { x = -40, y = 250, len = 200, wid = 50, angle = -45 }, --A
        { x = -150, y = 300, len = 200, wid = 50, angle = 90 }, --A
    },
    CHECKPOINTS = {},
    END = { x = -200, y = 500, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "Now it's harder, let's see how you do this one"
initLevel()

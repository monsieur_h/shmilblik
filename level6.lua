--==========
--LEVEL FILE
--LEVEL 6
--Kind of a tutorial
--==========
LEVEL = {
    SCORE = 0,
    NUMBER = 6,
    WALLS = {
        { x = -450, y = 300, len = 200, wid = 50, angle = 90 }, --A
    },
    CHECKPOINTS = {},
    END = { x = -250, y = 900, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "Click and Hold to place a first platform. Bounce on it to reach the flag."
--LEVEL.BACKGROUND    =   "0.png"
initLevel()


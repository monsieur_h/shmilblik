--==========================
--Utility functions
--==========================


--Clamps a value between two others.
function clamp(pValue, pMin, pMax)
    assert(pMin < pMax)
    if pValue > pMax then
        return pMax
    elseif pValue < pMin then
        return pMin
    else
        return pValue
    end
end

-- Print anything - including nested tables
function table_print(tt, indent, done)
    done = done or {}
    indent = indent or 0
    if type(tt) == "table" then
        for key, value in pairs(tt) do
            io.write(string.rep(" ", indent)) -- indent it
            if type(value) == "table" and not done[value] then
                done[value] = true
                io.write(string.format("[%s] => table\n", tostring(key)));
                io.write(string.rep(" ", indent + 4)) -- indent it
                io.write("(\n");
                table_print(value, indent + 7, done)
                io.write(string.rep(" ", indent + 4)) -- indent it
                io.write(")\n");
            else
                io.write(string.format("[%s] => %s\n",
                    tostring(key), tostring(value)))
            end
        end
    else
        io.write(tt .. "\n")
    end
end

--Wrapper for making animation wait and being more readable
function wait(action)
    if not action then
        return
    end
    while action:isBusy() do
        coroutine.yield()
    end
end


--Returns the size of a table (useful when not using numeric indexes)
function tableSize(pTable)
    local n = 0
    for k, v in pairs(pTable) do
        n = n + 1
    end
    return n
end

function tableGetAt(pTable, pNum)
    local n = 1
    for k, v in pairs(pTable) do
        if n == pNum then
            return v
        end
        n = n + 1
    end
    return nil
end

function distance(x1, y1, x2, y2)
    local dx = x1 - x2
    local dy = y1 - y2
    return math.sqrt(dx * dx + dy * dy)
end

function distance2(x1, y1, x2, y2)
    local dx = x1 - x2
    local dy = y1 - y2
    return (dx * dx + dy * dy)
end

function log(...)
    if GAME.DEBUG then
        print(...)
    end
end

function file_exists(name)
    local f = io.open(name, "r")
    if f ~= nil then
        io.close(f)
        return true
    else
        return false
    end
end

--==========
--LEVEL FILE
--LEVEL 8
--Kind of a tutorial
--==========
LEVEL = {
    SCORE = 0,
    NUMBER = 9,
    WALLS = {
        { x = -175, y = 100, len = 200, wid = 50, angle = -35 }, --A
        { x = 175, y = 500, len = 200, wid = 50, angle = 35 }, --A
        { x = -150, y = 230, len = 200, wid = 50, angle = 90 - 35, mode = "redbox" },
        { x = -10, y = 430, len = 200, wid = 50, angle = 90 - 35, mode = "redbox" },
    },
    CHECKPOINTS = {},
    END = { x = -500, y = 700, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "This one is very high"
--LEVEL.BACKGROUND    =   "0.png"
initLevel()





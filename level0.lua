--==========
--LEVEL FILE
--LEVEL 0
--Kind of a tutorial
--==========
LEVEL = {
    SCORE = 0,
    NUMBER = 0,
    WALLS = {},
    CHECKPOINTS = {},
    END = { x = -250, y = 250, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "Click and Hold to place a first platform. Bounce on it to reach the flag."
LEVEL.BACKGROUND = "0.png"
initLevel()

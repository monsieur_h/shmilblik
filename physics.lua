print('Starting physics simulation...')

--========================================
--Instanciate Physics World
--========================================
world = MOAIBox2DWorld.new()
world:setGravity(0, -30)
world:setUnitsToMeters(2 / 10)
world:start()

if GAME.DEBUG then
    layer:setBox2DWorld(world)
    world:setDebugDrawFlags(MOAIBox2DWorld.DEBUG_DRAW_SHAPES + MOAIBox2DWorld.DEBUG_DRAW_JOINTS +
            MOAIBox2DWorld.DEBUG_DRAW_PAIRS + MOAIBox2DWorld.DEBUG_DRAW_CENTERS)
    --MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX, 1, 1, 1, 1, 1 )
    --MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX_LAYOUT, 1, 0, 0, 1, 1 )
    --MOAIDebugLines.setStyle ( MOAIDebugLines.TEXT_BOX_BASELINES, 1, 1, 0, 0, 1 )
end

function deletePlayerPlatformProp(pProp)
    local t = MOAIThread.new()
    t:run(_deleteEffect, pProp)
end

function _deleteEffect(pProp)
    local x, y = pProp:getLoc()
    --wait( pProp:seekScl( x, y*5.5, 0.1 ) )
    --local x, y  =   playerBall:getPosition()
    --x, y        =   layer:worldToWnd( x, y )
    wait(pProp:seekColor(0, 0, 0, 0, 0.3, MOAIEaseType.EASE_OUT))
    layer:removeProp(pProp)
end

function onCollide(event, fixA, fixB, arbiter)
    if event == MOAIBox2DArbiter.BEGIN then
    end

    if event == MOAIBox2DArbiter.END then
        if fixB.tag == "player" then
            PLAYER.SOUNDS.HITBAR:stop()
            PLAYER.SOUNDS.HITBAR:play()
            PLAYER.AMMO = PLAYER.AMMO - 1
            PLAYER.SCORE = PLAYER.SCORE + 1
            updateClearColor(GAME.COLOR.BAD)
            deletePlayerPlatformProp(fixA:getBody().prop)
            fixA:getBody():destroy()
        end
    end

    if event == MOAIBox2DArbiter.PRE_SOLVE then
        print('pre!')
    end

    if event == MOAIBox2DArbiter.POST_SOLVE then
        print('post!')
    end
end

function reverseGravity()
    local x, y = world:getGravity()
    world:setGravity(-x, -y)
end

function resetGravity()
    local x, y = world:getGravity()
    if y >= 0 then
        reverseGravity()
    end
end

function playerCollision(event, fixA, fixB, arbiter)
    if event == MOAIBox2DArbiter.END and fixB.tag == "level" then
        PLAYER.SOUNDS.HITBLOCK:stop()
        PLAYER.SOUNDS.HITBLOCK:play()
        PLAYER.AMMO = 1
        updateText()
        updateClearColor(GAME.COLOR.GOOD)
        bumpwall(fixB:getBody())
        PLAYER.SKELETON.animationState:setAnimation("hit", false)

    elseif event == MOAIBox2DArbiter.END and fixB.tag == "magnet" then
        bumpwall(fixB:getBody())
        reverseGravity()
    elseif event == MOAIBox2DArbiter.END and fixB.tag == "redbox" then
        bumpwall(fixB:getBody())
        GAME.setState("LOOSE")
    end
end

--Fonction de création de platforme
barTexture = MOAIGfxQuad2D.new()
barTexture:setTexture("assets/playerBar.png")
barTexture:setRect(0, -15, 300, 15)
PLATFORMS = {}
function createPlatform(pX1, pY1, pX2, pY2)
    local width = 10
    local x = pX1 - pX2
    local y = pY1 - pY2
    local lenght = math.sqrt(x * x + y * y)
    local platform = world:addBody(MOAIBox2DBody.STATIC)
    local pFixture = platform:addRect(0, -width / 2, lenght, width / 2)
    pFixture:setFilter(0x1)
    pFixture:setRestitution(4.0)
    local pos = {
        x = pX1,
        y = pY1
    }
    --local angle =   y*(math.sqrt(x*x+y*y))
    --angle   =   angle
    local angle = math.atan2(x, y)
    angle = -math.deg(angle + math.pi / 2)

    platform:setTransform(pos.x, pos.y, angle)
    pFixture:setCollisionHandler(onCollide, MOAIBox2DArbiter.BEGIN + MOAIBox2DArbiter.END)
    local prop = MOAIProp2D.new()
    prop:setDeck(barTexture)
    --prop:setParent( platform )
    prop:setLoc(pX2, pY2)
    local iAngle = angle - 180
    iAngle = iAngle
    prop:setRot(iAngle)
    local scl = 1 / 300 * lenght
    prop:setScl(0, 1)
    layer:insertProp(prop)
    local x1, y1 = prop:getLoc()
    local a = prop:getRot()
    prop:seek(x1, y1, a, scl, 1, 0.4, MOAIEaseType.SOFT_SMOOTH)
    platform.prop = prop
    table.insert(PLATFORMS, pFixture)
end


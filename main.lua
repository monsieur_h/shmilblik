GAME = {
    NAME = "Super WhiteBoard Quest Adventure : The Game",
    STATE = "PRESTART",
    CURRENT_LEVEL = 0,
    DEBUG = false,
    COLOR = {
        GOOD = {
            r = 255 / 255,
            g = 255 / 255,
            b = 255 / 255,
            a = 255 / 255
        },
        BAD = {
            r = 205 / 255,
            g = 205 / 255,
            b = 255 / 255,
            a = 105 / 255
        }
    }
}

--DEBUG       =   true
require 'utils'
require 'graphics'
require 'physics'
--require 'player'
require 'thread'
require 'controls'
require 'levelbuilder'
print('Loading spine runtime...')
local spine = require "spine.spine"
print('Loading player...')

dofile("level0.lua")
PLAYER.loadSounds()
GAME.CURRENT_LEVEL = LEVEL.NUMBER

titleBox:setString(LEVEL.INTRO_TEXT or "You're doing great ! Keep going ! ")



function GAME.setState(pState)
    if GAME.STATE == pState then
        return
    end
    local tmpState = GAME.GAME --@TODO: Dirty hack so you can't loose when you already won
    GAME.PREVIOUS_STATE = GAME.STATE
    GAME.STATE = pState

    if pState == "WIN" then
        titleBox:setVisible(true)
        titleBox:setString("You finnished level " .. GAME.CURRENT_LEVEL .. ". Tap to try the next level")
        updateClearColor(GAME.COLOR.GOOD)
        PLAYER.SKELETON.animationState:setAnimation("win", false)
        showRays(true)
        camera.setMode("track")
        PLAYER.SOUNDS.WIN:stop()
        PLAYER.SOUNDS.WIN:play()
        --PLAYER.setActive( false )
    elseif pState == "LOOSE" then
        if GAME.PREVIOUS_STATE == "WIN" then
            GAME.STATE = GAME.PREVIOUS_STATE
            GAME.PREVIOUS_STATE = tmpState
            return
        end
        PLAYER.SOUNDS.LOOSE:stop()
        PLAYER.SOUNDS.LOOSE:play()
        titleBox:setVisible(true)
        titleBox:setString("You lost at level " .. LEVEL.NUMBER .. "\tTap to retry")
        updateClearColor(GAME.COLOR.BAD)
        showRays(false)
        camera.setMode("track")

    elseif pState == "NEXTLEVEL" then
        removePlayerPlatforms()
        local string = LEVEL.INTRO_TEXT or "Click and hold to place a first platform."
        titleBox:setString(string)
        log("Textbox should be " .. string)
        titleBox:setVisible(true)
        PLAYER.reset()
        PLAYER.setActive(false)
        resetCamera()
        PLAYER.AMMO = 1
        destroyLevel()
        nextLevel()
        resetGravity()
        updateClearColor(GAME.COLOR.GOOD)
        hideRays()
        resetCamera()
        GAME.setState("RUN")

    elseif pState == "RESTART" then
        PLAYER.SOUNDS.LOOSE:stop()
        removePlayerPlatforms()
        local string = LEVEL.INTRO_TEXT or "Click and hold to place a first platform."
        titleBox:setString(string)
        titleBox:setVisible(true)
        PLAYER.reset()
        clickedPoint = nil
        PLAYER.setActive(false)
        resetCamera()
        PLAYER.AMMO = 1
        PLAYER.SCORE = 0
        updateClearColor(GAME.COLOR.GOOD)
        resetGravity()
        hideRays()
        resetCamera()
        GAME.setState("RUN")

    elseif pState == "RUN" then
        titleBox:setVisible(true)
        local string = LEVEL.INTRO_TEXT or "Click and hold to place a first platform."
        titleBox:setString(string)
    end
    log("Game state is now : " .. GAME.STATE)
end

function removePlayerPlatforms()
    log("Destruction")
    if next(PLATFORMS) == nil then
        return
    end
    for i, v in pairs(PLATFORMS) do
        log("Destroying a platform")
        if v:getBody() ~= nil then
            deletePlayerPlatformProp(v:getBody().prop)
            v:getBody():destroy()
        end
    end
    PLATFORMS = {}
end

function nextLevel()
    log("Trying to load the next level...")
    log("Looking for level" .. (GAME.CURRENT_LEVEL + 1) .. ".lua")
    if file_exists("level" .. (GAME.CURRENT_LEVEL + 1) .. ".lua") then
        dofile("level" .. (GAME.CURRENT_LEVEL + 1) .. ".lua")
        GAME.CURRENT_LEVEL = LEVEL.NUMBER
        PLAYER.SCORE = 0
        log("Current level is now : " .. LEVEL.NUMBER)
    else
        log("Not found!")
        titleBox:setString("Congratulations the game is over")
        titleBox:setVisible(true)
        LEVEL.INTRO_TEXT = "Congratulations the game is over"
    end
end

--Création de la balle main player
playerBall = world:addBody(MOAIBox2DBody.DYNAMIC)
playerBall:setLinearDamping(0.1)
ballFixture = playerBall:addCircle(0, 0, 25)
ballFixture:setDensity(0.5)
ballFixture:setFriction(0.5)
ballFixture:setRestitution(0.95)
playerBall:resetMassData()
ballFixture.tag = "player"
ballFixture:setCollisionHandler(playerCollision, MOAIBox2DArbiter.BEGIN + MOAIBox2DArbiter.END)
playerBall:setActive(false)

--Texture/image
playerTexture = MOAIGfxQuad2D.new()
playerTexture:setTexture("assets/shmilblik.png")
playerTexture:setRect(-256, -256, 256, 256)
playerImage = MOAIProp2D.new()
playerImage:setDeck(playerTexture)
playerImage:setScl(0.16, 0.16)
playerImage:setParent(playerBall)
--layer:insertProp( playerImage )
table.insert(PLAYER.BODIES, playerBall)

--Arms
--LEFT
armTexture = MOAIGfxQuad2D.new()
armTexture:setTexture("assets/arm.png")
armTexture:setRect(0, -64, 128, 64)
leftArmBody = world:addBody(MOAIBox2DBody.DYNAMIC)
leftArmBody.initialPosition = {
    x = 30,
    y = 0,
    a = 0
}
leftArmBody:setActive(false)
leftArmFixture = leftArmBody:addRect(0, 0, 32, 4)
leftArmFixture:setFilter(0x02, 0x02)
leftArmBody:setTransform(30, 0, 0)
leftArmFixture:setDensity(0.5)
leftArmBody:setFixedRotation(false)
leftArmBody:setAngularDamping(10)
leftArmJoint = world:addRevoluteJoint(leftArmBody, playerBall, 32, 4)
leftArmJoint:setLimitEnabled(false)
leftArmImage = MOAIProp2D.new()
leftArmImage:setDeck(armTexture)
leftArmImage:setScl(0.32, 0.32)
leftArmImage:setParent(leftArmBody)
layer:insertProp(leftArmImage)
table.insert(PLAYER.BODIES, leftArmBody)

--RIGHT

--TODO:removethis
armTexture = MOAIGfxQuad2D.new()
armTexture:setTexture("assets/arm.png")
armTexture:setRect(0, -64, 128, 64)
rightArmBody = world:addBody(MOAIBox2DBody.DYNAMIC)
rightArmBody.initialPosition = {
    x = -30,
    y = 0,
    a = 180
}
rightArmBody:setActive(false)
rightArmFixture = rightArmBody:addRect(0, 0, 32, 4)
rightArmFixture:setFilter(0x02, 0x02)
rightArmBody:setTransform(-30, 0, 180)
rightArmFixture:setDensity(0.5)
rightArmBody:setFixedRotation(false)
rightArmBody:setAngularDamping(10)
rightArmJoint = world:addRevoluteJoint(rightArmBody, playerBall, -32, 4)
rightArmJoint:setLimitEnabled(false)
rightArmImage = MOAIProp2D.new()
rightArmImage:setDeck(armTexture)
rightArmImage:setScl(0.32, 0.32)
rightArmImage:setParent(rightArmBody)
layer:insertProp(rightArmImage)
table.insert(PLAYER.BODIES, rightArmBody)

--Setting up spine animations and skeleton
--========================================
local loader = spine.AttachmentLoader:new()
function loader:createImage(attachment)

    local deck = MOAIGfxQuad2D.new()
    --print("./assets/anim/" .. attachment.name .. ".png")
    deck:setTexture("./assets/anim/" .. attachment.name .. ".png")
    deck:setUVRect(0, 0, 1, 1)
    deck:setRect(0, 0, attachment.width, attachment.height)

    local prop = MOAIProp.new()
    prop:setDeck(deck)
    prop:setPiv(attachment.width / 2, attachment.height / 2)
    layer:insertProp(prop)

    return prop
end

local json = spine.SkeletonJson:new(loader)
json.scale = 0.17


local skeletonData = json:readSkeletonDataFile("./assets/anim/boogey/skeleton.json")
PLAYER.SKELETON = spine.Skeleton:new(skeletonData)
PLAYER.SKELETON.flipX = false
PLAYER.SKELETON.flipY = true
if GAME.DEBUG then
    --PLAYER.SKELETON.debugBones  =   true
end
PLAYER.SKELETON.debugLayer = layer
PLAYER.SKELETON:setToBindPose()
PLAYER.SKELETON.animationStateData = spine.AnimationStateData:new(skeletonData)
PLAYER.SKELETON.animationState = spine.AnimationState:new(PLAYER.SKELETON.animationStateData)
PLAYER.SKELETON.animationState:setAnimation("idle", true)
PLAYER.SKELETON.animationStateData:setMix("idle", "hit", 0.2)
PLAYER.SKELETON.animationStateData:setMix("hit", "fly", 0.2)
PLAYER.SKELETON.animationStateData:setMix("fly", "idle", 0.5)
PLAYER.SKELETON.animationStateData:setMix("fly", "idle", 0.5)
PLAYER.SKELETON.animationStateData:setMix("idle", "win", 0.5)
PLAYER.SKELETON.animationStateData:setMix("win", "idle", 0.5)




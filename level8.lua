--==========
--LEVEL FILE
--LEVEL 8
--Kind of a tutorial
--==========
LEVEL = {
    SCORE = 0,
    NUMBER = 8,
    WALLS = {
        { x = -550, y = 0, len = 200, wid = 50, angle = 90 }, --A
        { x = 550, y = 500, len = 200, wid = 50, angle = 90 }, --A
    },
    CHECKPOINTS = {},
    END = { x = -500, y = 900, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "A hard one ! "
--LEVEL.BACKGROUND    =   "0.png"
initLevel()




print('Loading graphics...')
print("Starting MOAI on : " .. MOAIEnvironment.osBrand)
if MOAIEnvironment.osVersion then
    print("OS version : " .. MOAIEnvironment.osVersion)
end
if MOAIEnvironment.screenWidth then
    print("Detected resolution : " .. MOAIEnvironment.screenWidth .. 'x' .. MOAIEnvironment.screenHeight)
end

if AUDIO then
    print("Starting sound engine...")
    MOAIUntzSystem.initialize()
    MOAIUntzSystem.setVolume(1)
end

Screen = {
    w = MOAIEnvironment.screenWidth or 1280,
    h = MOAIEnvironment.screenHeight or 768
}

Stage = {
    w = 1280,
    h = 768
}

MOAISim.openWindow(GAME.NAME, Screen.w, Screen.h)

--Camera settings
camera = MOAICamera2D.new()
camera.maxZoom = 1.3
camera.minZoom = 1.0
camera.mode = 'none'
camera:setScl(1, 1)
camera.mode = "play" --or track
camera.default = {
    scl = { x = 1, y = 1 },
}

function updateCameraPos(pX, pY)
    if camera.mode == "play" then
        local x, y = camera:getLoc()
        if pY - Stage.h / 5 > y then
            camera:setLoc(x, pY - Stage.h / 5)
        end
    elseif camera.mode == "track" then
        camera:setLoc(playerBall:getPosition())
        --camera:seekLoc( playerBall:getPosition(), 0.5 )
    end
end

function camera.setMode(pMode)
    if pMode == "track" then
        camera:seekScl(0.5, 0.5, 0.8)
        camera.mode = pMode
    end
end

function resetCamera()
    camera.mode = "play"
    camera:setScl(camera.default.scl.x, camera.default.scl.y)
    camera:setLoc(0, 0)
end

function easeCamera(...)
    wait(camera:move(...))
end

viewport = MOAIViewport.new()
viewport:setSize(Screen.w, Screen.h)
viewport:setScale(Stage.w, Stage.h)


--========================================
--Setting up layers
--========================================
hudLayer = MOAILayer2D.new()
hudLayer:setViewport(viewport)

layer = MOAILayer2D.new()
layer:setViewport(viewport)
layer:setCamera(camera)

backgroundLayer = MOAILayer2D.new()
backgroundLayer:setViewport(viewport)
foregroundLayer = MOAILayer2D.new()
foregroundLayer:setViewport(viewport)
foregroundLayer:setCamera(camera)
--backgroundLayer:setCamera( camera )
layerTable = {
    backgroundLayer,
    foregroundLayer, --Utile? Rly?
    layer,
    hudLayer
}
MOAIRenderMgr.setRenderTable(layerTable)

--========================================
--HUD
--========================================
charcodes = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 .,:;!?()&/-'

dejavuFont = MOAIFont.new()
dejavuFont:load('fonts/DOVES.TTF')
dejavuFont:preloadGlyphs(charcodes, 24)

whiteFontStyle = MOAITextStyle.new()
whiteFontStyle:setFont(dejavuFont)
whiteFontStyle:setSize(55)

--Topleft TextBox
textBox = MOAITextBox.new()
textBox:setScl(0.5, 0.5)
--textBox:setRect( -100, -150, 100, 150 )
--textBox:setRect( -600, -80, 600, 80 )
textBox:setRect(10, 0, 1200, 160)
textBox:setLoc(-Stage.w / 2, Stage.h / 2 - 70)
textBox:setStyle(whiteFontStyle)
textBox:setAlignment(MOAITextBox.LEFT_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
textBox:setYFlip(true)
textBox:setString("Click and hold to place a first platform.")
textBox:setColor(0, 0, 0, 1)
textBox:setVisible(false)
hudLayer:insertProp(textBox)

titleBox = MOAITextBox.new()
titleBox:setScl(1, 1)
titleBox:setRect(-175, -200, 200, 175)
titleBox:setStyle(whiteFontStyle)
titleBox:setAlignment(MOAITextBox.LEFT_JUSTIFY, MOAITextBox.CENTER_JUSTIFY)
titleBox:setYFlip(true)
titleBox:setVisible(true)
titleBox:setLoc(Stage.w / 3 - 100, Stage.h / 3)
titleBox:setColor(0, 0, 0, 1)
--titleBox:setString( "Click and hold to place a first platform." )
hudLayer:insertProp(titleBox)

function updateText()
    textBox:setString("Perfect Win :\t" .. LEVEL.SCORE .. "\nYou did :\t" .. PLAYER.SCORE)
    if GAME.STATE ~= "WIN" or GAME.STATE ~= "LOOSE" then
        titleBox:setVisible(false)
    end
    PLAYER.previousAMMO = PLAYER.AMMO
    local t = MOAIThread.new()
    t:run(tiltText)
end

function tiltText()
    wait(textBox:moveLoc(1.0, 1.0, 1.0, 0.2))
    wait(textBox:moveLoc(-1.0, -1.0, -1.0, 0.2))
end

function bumpText()
    local t = MOAIThread.new()
    t:run(_bumpText.prop)
end

function _bumpText()
    wait(textBox:moveScl(0.2, 0.2, 0.2, 0.2))
    wait(textBox:moveScl(-0.2, -0.2, -0.2, 0.2))
end

COLOR = GAME.COLOR.GOOD

rectangleDeck = MOAIScriptDeck.new()
rectangleDeck:setRect(-Stage.w / 2, -Stage.h / 2, Stage.w / 2, Stage.h / 2)
rectangleDeck:setDrawCallback(function()
    MOAIGfxDevice.setPenColor(COLOR.r, COLOR.g, COLOR.b, COLOR.a)
    MOAIDraw.fillRect(-Stage.w / 2, -Stage.h / 2, Stage.w / 2, Stage.h / 2)
end)
backProp = MOAIProp2D.new()
backProp:setDeck(rectangleDeck)
backgroundLayer:insertProp(backProp)


winRayTexture = MOAIGfxQuad2D.new()
winRayTexture:setTexture("assets/winrays.png")
winRayTexture:setRect(-256, -256, 256, 256)
winRayProp = MOAIProp2D.new()
winRayProp:setDeck(winRayTexture)
winRayProp:setColor(0, 0, 0, 0)
foregroundLayer:insertProp(winRayProp)

pointerTexture = MOAIGfxQuad2D.new()
pointerTexture:setTexture("assets/arrow.png")
pointerTexture:setRect(-128, -128, 128, 128)
pointerProp = MOAIProp2D.new()
pointerProp:setDeck(pointerTexture)
foregroundLayer:insertProp(pointerProp)

--looseRayTexture =   MOAIGfxQuad2D.new()
--looseRayTexture:setTexture( "assets/looserays.png" )
--looseRayTexture:setRect( -256, -256, 256, 256 )
--looseRayProp    =   MOAIProp2D.new()
--looseRayProp:setDeck( looseRayTexture )
--looseRayProp:setColor( 0, 0, 0, 0 )
--foregroundLayer:insertProp( looseRayProp )

function updateClearColor(pColor)
    COLOR = pColor
end

function showRays(pWin)
    if pWin then --Show rays for winning the game
    winRayProp:seekColor(1, 1, 1, 1, 0.5)
    else --Show rays for loosing the game
    --looseRayProp:seekColor( 1, 1, 1, 1, 0.5 )
    end
end

function updateRays(pDt, pX, pY)
    local rotSpeed = 180
    winRayProp:setLoc(pX, pY)
    winRayProp:moveRot(rotSpeed * pDt)
    --Update the green arrow
    if LEVEL.END.BODY then
        local x, y = LEVEL.END.BODY:getPosition()
        x = x + LEVEL.END.wid / 2
        y = y + LEVEL.END.len / 2
        directionX = pX - x
        directionY = pY - y
        local angle = math.atan2(directionX, directionY)
        angle = -math.deg(angle + math.pi)
        pointerProp:setRot(angle)
    end
    pointerProp:setLoc(pX, pY)
    --looseRayProp:setLoc( pX, pY )
    --looseRayProp:moveRot( rotSpeed * pDt )
end

function hideRays()
    winRayProp:seekColor(0, 0, 0, 0, 0.2)
    --looseRayProp:seekColor( 0, 0, 0, 0, 0.2 )
end

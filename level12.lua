--==========
--LEVEL FILE
--LEVEL 12
--Kind of a tutorial
--==========
LEVEL = {
    SCORE = 0,
    NUMBER = 12,
    WALLS = {
        { x = -500, y = 100, len = 200, wid = 50, angle = 45, mode = "magnet" },
        { x = -150, y = 0, len = 200, wid = 50, angle = 90, mode = "redbox" }, --A
        { x = -150, y = -400, len = 200, wid = 50, angle = 90, mode = "redbox" }, --A
        { x = -150, y = 200, len = 200, wid = 50, angle = 90, mode = "redbox" }, --A
        { x = -25, y = 300, len = 200, wid = 50, angle = 0, mode = "redbox" }
    },
    CHECKPOINTS = {},
    END = { x = -400, y = 700, len = 200, wid = 100, angle = 0 },
}
LEVEL.FIXTURES = {}
LEVEL.INTRO_TEXT = "You can't win this one"
--LEVEL.BACKGROUND    =   "0.png"
initLevel()






